import discord
import game
#from dot_env import load_dotenv


#load_dotenv()
token = 'ODI0ODI0OTQ2MTU2NzY1MjE1.YF1AFw.lfWuJOL7ba1huWTYy1adW9u3ZVE'

client = discord.Client()



@client.event
async def on_ready():
    print(f'{client.user} has connected to discord')

async def send_game_state(game, message):
    game.save_board_image('exported.png')
    #print('Exporting please?')
    color = game.turn_color()
    if (color == 'white'):
        status_str = 'White to move'
    else:
        status_str = 'Black to move'

    with open('exported.png', 'rb') as pic:
        picture = discord.File(pic)
        #await message.channel.send('Hello!')
        await message.channel.send(status_str, file=picture)

game_inst = None
@client.event
async def on_message(message):
    #print(message.content)
    body = message.content.split(' ')
    #header = message.content.split(' ', 1)
    header = body[0]
    global game_inst
    print('Got message')
    print(body)
    if (header == 'kc' or header == 'kchess'):
        #await message.channel.send("hi back!")
        if (body[1] == 'start'):
            game_inst = game.Game()
            await send_game_state(game_inst, message)
        elif(body[1] == 'draw'):
            if (game_inst != None):
                game_inst.save_board_image('exported.png')
                await send_game_state(game_inst, message)
            else:
                await message.channel.send('Game not initialized, please do so with kc start')
        elif (game_inst != None and body[1] != None):
            if (game_inst.parse_move(body[1])):
                game_inst.save_board_image('exported.png')
                await send_game_state(game_inst, message)
            else:
                await message.channel.send('Invalid Move')
        elif (body[1] == 'help'):
            help_str = '''
            kc [move] -- Moves a piece
            kc help -- Displays this message
            kc start -- Starts a game
            kc draw -- Draws the game state

            Moves are accepted in either algebraic or normal notation.
            If you want to move a piece from e2 to e4 in algebraic notation,
            you do not need to specify the piece, simply type 'e2e4' without
            spacing. If you want to move any piece other than a pawn in normal
            notation, use characters rnbqk, and then specify the destination
            space. If you want to specify an input file or rank, place the file
            or rank before the piece identifier. Explicitely specifying a capture is
            optional, and done by saying x after the piece specifier. For example:
            e4      -- Move pawn to e4
            pd6     -- Move pawn to d6
            dxe5    -- Move pawn on file d to e5, and capture a piece
            Ke4     -- Move king to e4
            Ba2     -- Move Bishop to a2
            bRxb6   -- Move Rook at file b to b6, and capture a piece
            Nxa4b6  -- Move Knight from a4 to b6, capture a piece
            a4b6    -- Move any piece from a4 to b6
            0-0     -- Castle on king side
            0-0-0   -- Castle on queen side
            '''
            await message.channel.send(help_str)
        print('Got KC Message')

client.run(token)