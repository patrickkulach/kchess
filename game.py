import string
import datetime
from PIL import Image

# Character set:
# - Empty
# p Pawn
# r Rook
# n Knight
# b Bishop
# q Queen
# k Kang
# White is upper case, black is lowercase
# To find rank 7 on file c, use self.board[6*8 + 2]

image_bk = Image.open('res/blackKing.png').convert('RGBA')
image_bp = Image.open('res/blackPawn.png').convert('RGBA')
image_br = Image.open('res/blackRook.png').convert('RGBA')
image_bb = Image.open('res/blackBishop.png').convert('RGBA')
image_bn = Image.open('res/blackKnight.png').convert('RGBA')
image_bq = Image.open('res/blackQueen.png').convert('RGBA')
image_wk = Image.open('res/whiteKing.png').convert('RGBA')
image_wp = Image.open('res/whitePawn.png').convert('RGBA')
image_wr = Image.open('res/whiteRook.png').convert('RGBA')
image_wb = Image.open('res/whiteBishop.png').convert('RGBA')
image_wn = Image.open('res/whiteKnight.png').convert('RGBA')
image_wq = Image.open('res/whiteQueen.png').convert('RGBA')

class Game:
    def __init__(self):
        self.board = ['-'] * 64
        # Fill the game
        self.board[8:16] = 'P' * 8
        self.board[48:56] = 'p' * 8

        self.board[0] = 'R'
        self.board[7] = 'R'
        self.board[56] = 'r'
        self.board[63] = 'r'

        self.board[1] = 'N'
        self.board[6] = 'N'
        self.board[57] = 'n'
        self.board[62] = 'n'

        self.board[2] = 'B'
        self.board[5] = 'B'
        self.board[58] = 'b'
        self.board[61] = 'b'

        self.board[3] = 'Q'
        self.board[59] = 'q'
        self.board[4] = 'K'
        self.board[60] = 'k'
        self.castle_wk = True
        self.castle_wq = True
        self.castle_bk = True
        self.castle_bq = True
        self.turn = 'white'

        now = datetime.datetime.now()
        self.log_file_path = now.strftime('log/%Y%m%d%H%M%S.log')
        self.log_file = open(self.log_file_path, 'a')
        datestr = now.strftime('%Y-%m-%d %H:%m:%S')
        self.log_file.write('New game created at ' +  datestr + '\n')
        self.log_file.close()


    # White and black pawns have different available moves
    def move_p(self, sr, sf, dr, df):
        # TODO -- Add en pessant
        piece = self.piece_on(sr, sf)
        piece_d = self.piece_on(dr, df)
        if (piece[0] == 'p' and piece[1] == self.turn):
            if (piece_d[1] == 'none' and (dr - sr) == 1 and sf == df and self.turn == 'white'):
                # Normal 1 move, white
                self.piece_move(sr, sf, dr, df)
                return True
            elif (piece_d[1] == 'none' and (dr - sr) == -1 and sf == df and self.turn == 'black'):
                self.piece_move(sr, sf, dr, df)
                return True
            elif (piece_d[1] == 'none' and (dr - sr) == 2 and sr == 1 and sf == df and self.turn == 'white' and self.free_straight(sr, sf, dr, df)):
                self.piece_move(sr, sf, dr, df)
                return True
            elif (piece_d[1] == 'none' and (dr - sr) == -2 and sr == 6 and sf == df and self.turn == 'black' and self.free_straight(sr, sf, dr, df)):
                self.piece_move(sr, sf, dr, df)
                return True
            elif (piece_d[1] =='black' and (dr - sr) == 1 and abs(sf - df) == 1 and self.turn == 'white'):
                self.piece_move(sr, sf, dr, df)
                return True
            elif (piece_d[1] == 'white' and (dr - sr) == -1 and abs(sf - df) == 1 and self.turn == 'black'):
                self.piece_move(sr, sf, dr, df)
                return True
        return False
    # Knight, bishop, queen, and rook are all the same
    def move_n(self, sr, sf, dr, df):
        piece_s = self.piece_on(sr, sf)
        piece_d = self.piece_on(dr, df)
        if (piece_s[1] == self.turn and piece_s[0] == 'n' and (piece_d[1] == self.enemy_color() or piece_d[1] == 'none')):
            deltar = abs(sr - dr)
            deltaf = abs(sf - df)
            if ((deltar == 2 and deltaf == 1) or (deltar == 1 and deltaf == 2)):
                self.piece_move(sr, sf, dr, df)
                return True
        return False
    def move_b(self, sr, sf, dr, df):
        piece_s = self.piece_on(sr, sf)
        piece_d = self.piece_on(dr, df)
        if (not self.is_diag(sr, sf, dr, df)):
            return False
        if (piece_s[0] == 'b' and piece_s[1] == self.turn and (piece_d[1] == self.enemy_color() or piece_d[1] == 'none')):
            if (self.free_diag(sr, sf, dr, df)):
                self.piece_move(sr, sf, dr, df)
                return True
            
        return False
    def move_q(self, sr, sf, dr, df):
        piece_s = self.piece_on(sr, sf)
        piece_d = self.piece_on(dr, df)
        if (not self.is_diag(sr, sf, dr, df) and (sr != dr) and (sf != df)):
            return False
        if (piece_s[0] == 'q' and piece_s[1] == self.turn and (piece_d[1] == self.enemy_color() or piece_d[1] == 'none')):
            if (self.free_diag(sr, sf, dr, df)):
                self.piece_move(sr, sf, dr, df)
                return True
            elif ((sr == dr) or (sf == df)):
                if (self.free_straight(sr, sf, dr, df)):
                    self.piece_move(sr, sf, dr, df)
                    return True
        return False
    def move_r(self, sr, sf, dr, df):
        piece_s = self.piece_on(sr, sf)
        piece_d = self.piece_on(dr, df)
        if ((sr != dr) and (sf != df)):
            return False
        if (piece_s[0] == 'r' and piece_s[1] == self.turn and (piece_d[1] == self.enemy_color() or piece_d[1] == 'none')):
            if (self.free_straight(sr, sf, dr, df)):
                self.piece_move(sr, sf, dr, df)
                
                # Invalidate castling moves
                if (sr == 0 and sf == 0):
                    self.castle_wq = False
                elif (sr == 0 and sf == 7):
                    self.castle_wk = False
                elif (sr == 7 and sf == 0):
                    self.castle_bq = False
                elif (sr == 7 and sf == 7):
                    self.castle_bk = False

                return True
        return False
    # King movement must check for everybody

    def move_k(self, sr, sf, dr, df):
        # TODO Check if you are in the kill range of any black piece
        piece_s = self.piece_on(sr, sf)
        piece_d = self.piece_on(dr, df)
        if (abs(sr - dr) > 1 or abs(sf - df) > 1):
            return False
        if (piece_s[0] == 'k' and piece_s[1] == self.turn and (piece_d[1] == self.enemy_color() or piece_d[1] == 'none')):
            self.piece_move(sr, sf, dr, df)
            if (self.turn == 'white'):
                self.castle_wk = False
                self.castle_wq = False
            else:
                self.castle_bk = False
                self.castle_bq = False
            return True
        return False

    def castle(self, type):
        if (type == 'king' and self.turn == 'white' and self.castle_wk):
            if (self.free_straight(0, 4, 0, 7)):
                # First rook, then king
                self.piece_move(0, 7, 0, 5)
                self.piece_move(0, 4, 0, 6)
                self.castle_wk = False
                self.castle_wq = False
                return True
            else:
                return False
        elif (type == 'queen' and self.turn == 'white' and self.castle_wq):
            if (self.free_straight(0, 0, 0, 4)):
                self.piece_move(0, 0, 0, 3)
                self.piece_move(0, 4, 0, 2)
                self.castle_wk = False
                self.castle_wq = False
                return True
            else:
                return False
        elif (type == 'king' and self.turn == 'black' and self.castle_bk):
            if (self.free_straight(7, 4, 7, 7)):
                # First rook, then king
                self.piece_move(7, 7, 7, 5)
                self.piece_move(7, 4, 7, 6)
                self.castle_bk = False
                self.castle_bq = False
                return True
            else:
                return False
        elif (type == 'queen' and self.turn == 'black' and self.castle_bq):
            if (self.free_straight(7, 0, 7, 4)):
                self.piece_move(7, 0, 7, 3)
                self.piece_move(7, 4, 7, 2)
                self.castle_bk = False
                self.castle_bq = False
                return True
            else:
                return False


    def enemy_color(self):
        if (self.turn == 'white'):
            return 'black'
        else:
            return 'white'
    def turn_color(self):
        return self.turn
    def free_straight(self, sr, sf, dr, df):
        if (sr == dr):
            high_f = max(sf, df)
            low_f = min(sf, df)
            if (high_f - low_f <= 1):
                return True
            for f in range(low_f + 1, high_f):
                if (self.board[8*sr+f] != '-'):
                    return False
        elif (sf == df):
            high_r = max(sr, dr)
            low_r = min(sr, dr)
            if (high_r - low_r <= 1):
                return True
            for r in range(low_r + 1, high_r):
                if (self.board[8*r+sf] != '-'):
                    return False
        else:
            return False
        return True

    def is_diag(self, sr, sf, dr, df):
        return (abs(sr-dr) == abs(sf - df))
    
    def free_diag(self, sr, sf, dr, df):
        deltar = dr - sr
        deltaf = df - sf
        # Check if it actually is a diagnal
        if (abs(deltar) != abs(deltaf)):
            return False
        if (abs(deltar) <= 1):
            return True

        if (deltar > 0):
            stepr = 1
        else:
            stepr = -1
        if (deltaf > 0):
            stepf = 1
        else:
            stepf = -1
        for i in range(abs(deltar) - 1):
            r = sr + stepr * (i+1)
            f = sf + stepf * (i+1)
            if (self.board[8*r+f] != '-'):
                return False
        return True

    def piece_on(self, r, f):
        piece = self.board[8*r+f]

        if (piece == '-'):
            color = 'none'
        elif (piece in 'prnbqk'):
            color = 'black'
        elif (piece in 'PRNBQK'):
            color = 'white'
        return (piece.lower(), color)

    def piece_move(self, sr, sf, dr, df):
        piece = self.board[8*sr+sf]
        self.board[8*sr+sf] = '-'
        self.board[8*dr+df] = piece        
    
    def next_turn(self):
        print('changing turn')
        if (self.turn == 'white'):
            self.turn = 'black'
        else:
            self.turn = 'white'

    def log_command(self, command):
        self.log_file = open(self.log_file_path, 'a')
        self.log_file.write(command + '\n')
        self.log_file.close()

    def parse_move(self, move):
        # On invalid
        move = move.lower()
        move = move.replace(' ', '').strip()
        self.log_command(move)
        if (len(move) > 7 or len(move) == 0):
            return False
        
        # Explicitly stated parameters
        rank = -1
        file = -1
        destroy = False
        piece = '-'
        

        # Check for overriding invalid move, useful for en pessant
        success = False
        print(move)
        if (move[0:3] == '!!!'):
            if (move[3] in 'abcdefgh'):
                sf = ord(move[3]) - ord('a')
            else:
                return False
            if (move[4] in '12345678'):
                sr = int(move[4]) - 1
            else:
                return False
            if (move[5] in 'abcdefgh'):
                df = ord(move[5]) - ord('a')
            else: 
                return False
            if (move[6] in '12345678'):
                dr = int(move[6]) - 1
            else:
                return False
            if (len(move) != 7):
                return False
            self.piece_move(sr, sf, dr, df)
            return True
            
        # Check for castle 
        if (move == '0-0'):
            success = self.castle('king')
        elif(move == '0-0-0'):
            success = self.castle('queen')
        if (success):
            self.next_turn()
            return True
        # Explicitly given piece
        if (move[0] in 'rnbqkp'):
            piece = move[0]
            move = move[1:]
            explicit_piece = True
        else:
            piece = 'p'
            explicit_piece = False
        '''
        if (len(move) >= 4):
            if (move[0] in 'abcdefgh'):
                file = ord(move[0]) - ord('a')
            else:
                return False

            if (move[1] in '12345678'):
                rank = int(move[1]) - 1
            else:
                return False
            move = move[2:]
            '''
        # This occurs if the player explicitly gives a rank or file
        if (len(move) >= 3):
            if(move[0] in 'abcdefgh'):
                file = ord(move[0])- ord('a')
                move = move[1:]
            if (move[0] in '12345678'):
                rank = int(move[0]) - 1
                move = move[1:]
        # Check if the player is using algebraic notation
        if (rank != -1 and file != -1):
            if (explicit_piece):
                if (piece != self.piece_on(rank, file)[0]):
                    print('Wrong piece specified, abandoning move')
                    return False 
            else:
                piece = self.piece_on(rank, file)[0]
            
        # Explicitly killing a piece
        if (move[0] == 'x'):
            destroy = True
            move = move[1:]
         
        
        # Final check -- are the last two characters the destination
        if (len(move) == 2):
            if (move[0] in 'abcdefgh'):
                dest_file = ord(move[0]) - ord('a')
            else:
                return False

            if (move[1] in '12345678'):
                dest_rank = int(move[1]) - 1
            else:
                return False
        else:
            return False

        #dest = 8*dest_rank + dest_file
        if (rank == -1 and file == -1):
            file_range = range(8)
            rank_range = range(8)
        elif (rank != -1 and file != -1):
            file_range = range(file, file+1)
            rank_range = range(rank, rank+1)
        elif (rank != -1):
            file_range = range(8)
            rank_range = range(rank, rank+1)
        elif (file != -1):
            file_range = range(file, file+1)
            rank_range = range(8)
        
        success = False
        for r in rank_range:
            for f in file_range:
                #print(r, f)
                #source = 8 * r + f
                #success = False
                if (piece == 'p'):
                    success = self.move_p(r, f, dest_rank, dest_file)
                elif (piece == 'r'):
                    success = self.move_r(r, f, dest_rank, dest_file)
                elif (piece == 'n'):
                    success = self.move_n(r, f, dest_rank, dest_file)
                elif (piece == 'b'):
                    success = self.move_b(r, f, dest_rank, dest_file)
                elif (piece == 'q'):
                    success = self.move_q(r, f, dest_rank, dest_file)
                elif (piece == 'k'):
                    success = self.move_k(r, f, dest_rank, dest_file)
                
                if (success):
                    break
            if (success):
                break
        print(success)
        if (success):
            self.next_turn()
        print('Piece', piece, 'Rank', rank, 'File', file, 'Kill?', destroy, 'Dest:', dest_file, dest_rank)
        # Lets actually try the move now 
        # On valid
        return success

    def print_board(self):
        for i in reversed(range(8)):
            print(i+1, end='   ')
            for j in range(8):
                print(self.board[8*i+j], end=' ')
            print('')
        print('')
        print('    a b c d e f g h')

    def save_board_image(self, outfile):
        #back_image = board_image
        #back_image = Image.open('res/board_green_400.png').convert('RGBA')
        back_image = Image.open('res/blue2.png').convert('RGBA')
        for r in range(8):
            for f in range(8):
                position = (50*f, 50*(7-r))
                c = self.board[8*r+f]
                if (c == 'P'):
                    im = image_wp
                elif (c == 'R'):
                    im = image_wr
                elif (c == 'N'):
                    im = image_wn
                elif (c == 'B'):
                    im = image_wb
                elif (c == 'Q'):
                    im = image_wq
                elif (c == 'K'):
                    im = image_wk
                elif (c == 'p'):
                    im = image_bp
                elif (c == 'r'):
                    im = image_br
                elif (c == 'n'):
                    im = image_bn
                elif (c == 'b'):
                    im = image_bb
                elif (c == 'q'):
                    im = image_bq
                elif (c == 'k'):
                    im = image_bk

                if (c != '-'):
                    back_image.paste(im, position, im)
        back_image.save(outfile, 'PNG')
                




if __name__ == '__main__':
    
    g = Game()
    while True:
        stri = input()
        g.parse_move(stri)
        g.print_board()
        g.save_board_image('exported.png')
    
    '''
    image_bb.convert('RGBA')
    board_image.convert('RGBA')
    board_image.paste(image_bb, (0, 0), image_bb)
    board_image.save('exported.png', 'PNG')
    '''